sugain qbal=1  < su/$1.su \
| supswigp xcur=1.5  title="$1 (trace normalized)" key=gx \
label2=" X [m]" label1="Time [s]" wbox=6.0 hbox=4.0  \
linewidth=0 titlesize=16 style=seismic clip=1.0  \
d1num=0.1 d2num=20  > ps/$1.ps
 
 
ghostview  ps/$1.ps &
