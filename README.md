# What is SOFI2D_sh?

SOFI2D_sh stands for Seismic mOdeling with FInite differences and denotes our 2-D viscoelastic time domain massive parallel modeling code for SH-waves.   
Right now, SOFI2D_sh is in an unsupported alpha-stage. The code comes as it is, no manual is provided (though it would be similar to the [SOFI2D_guide](https://git.scc.kit.edu/GPIAG-Software/SOFI2D/wikis/home)),
no tests have been performed. We also do not use this code for production. Please expect problems running the code!

# Download and Newsletter

You can Download the [latest stable Release](https://git.scc.kit.edu/GPIAG-Software/SOFI2D_sh/tree/Release) or the current [beta-version](https://git.scc.kit.edu/GPIAG-Software/SOFI2D_sh/tree/master).
