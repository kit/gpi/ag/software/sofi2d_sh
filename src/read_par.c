/*------------------------------------------------------------------------
 * Copyright (C) 2011 For the list of authors, see file AUTHORS.
 *
 * This file is part of SOFI2D_sh.
 * 
 * SOFI2D_sh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * SOFI2D_sh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SOFI2D_sh. See file COPYING and/or 
  * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/
/**/
/*------------------------------------------------------------------------
 *   Read FD-Parameters from Stdin                           
 *   last update 05/04/2002
 *
 *  T. Bohlen
 *  ----------------------------------------------------------------------*/

/* reading input-parameter from input-file or stdin for
viscoelastic finite-differnce modelling with fdveps */

#include "fd.h"

void read_par(FILE *fp_in){

/* declaration of extern variables */
extern int   NX, NY, QUELLART, SNAP, SNAP_FORMAT, L;
extern float DH, TIME, DT, TS, *FL, TAU, DAMPING, PLANE_WAVE_DEPTH, PHI;
extern float XREC1, XREC2, YREC1, YREC2;
extern float REC_ARRAY_DEPTH, REC_ARRAY_DIST;
extern int SEISMO, NDT, NGEOPH, SEIS_FORMAT, FREE_SURF, READMOD, READREC, SRCREC;
extern int BOUNDARY, REC_ARRAY, DRX, LOG;
extern float TSNAP1, TSNAP2, TSNAPINC, FW, REFREC[4];
extern char  MFILE[STRING_SIZE], SIGNAL_FILE[STRING_SIZE], LOG_FILE[STRING_SIZE];
extern char SNAP_FILE[STRING_SIZE], SOURCE_FILE[STRING_SIZE], REC_FILE[STRING_SIZE];
extern char SEIS_FILE_VZ[STRING_SIZE];
extern int  NPROCX, NPROCY, MYID; 

/* definition of local variables */
char s[74];
int  c=0, lineno=0, l;



if (MYID == 0){ 
   FL=vector(1,L);
   while ((c=getc(fp_in)) != EOF){
      if ((c=='\n') && (getc(fp_in)!='#')){     
	 lineno++;
	 // printf(" reading line %d \n",lineno);
	 switch (lineno){
	 case 1 :
	    fscanf(fp_in,"%s =%i",s,&NPROCX);
	    break;
	 case 2 :
	    fscanf(fp_in,"%s =%i",s,&NPROCY);
	    break;     		
	 case 3 :
	    fscanf(fp_in,"%s =%i",s,&NX);
	    break;
	 case 4 :
	    fscanf(fp_in,"%s =%i",s,&NY);
	    break;
	 case 5 :
	    fscanf(fp_in,"%s =%f",s,&DH);
	    break;
	 case 6 :
	    fscanf(fp_in,"%s =%f",s,&TIME);
	    break;
	 case 7 :
	    fscanf(fp_in,"%s =%f",s,&DT);
	    break;
	 case 8 :
	    fscanf(fp_in,"%s =%i",s,&QUELLART);
	    break;
	 case 9 :
	    fscanf(fp_in,"%s =%f",s,&PLANE_WAVE_DEPTH);
	    break;
	 case 10 :
	    fscanf(fp_in,"%s =%f",s,&PHI);
	    break;
	 case 11 :
	    fscanf(fp_in,"%s =%s",s,SIGNAL_FILE);
	    break;
	 case 12 :
	    fscanf(fp_in,"%s =%f",s,&TS);
	    break;
	 case 13 :
	    fscanf(fp_in,"%s =%i",s,&SRCREC);
	    break;
	 case 14 :
	    fscanf(fp_in,"%s =%s",s,SOURCE_FILE);
	    break;
	 case 15 :
	    fscanf(fp_in,"%s =%i",s,&READMOD);
	    break;
	 case 16 :
	    fscanf(fp_in,"%s =%s",s,MFILE);
	    break;
	 case 17 :
	    fscanf(fp_in,"%s =%i",s,&L);
	    break;
	 case 18 :
	    fscanf(fp_in,"%s =%f",s,&FL[1]);
	    for (l=2;l<=L;l++) fscanf(fp_in,"%f",&FL[l]);
	    break;
	 case 19 :
	    fscanf(fp_in,"%s =%f",s,&TAU);
	    break;
	 case 20 :
	    fscanf(fp_in,"%s =%i",s,&FREE_SURF);
	    break;
	 case 21 :
	    fscanf(fp_in,"%s =%f",s,&FW);
	    if (FW<0.0) FW=0.0;
	    break;
	 case 22 :
	    fscanf(fp_in,"%s =%f",s,&DAMPING);
	    break;			
	 case 23 :
	    fscanf(fp_in,"%s =%i",s,&BOUNDARY);
	    break;			
	 case 24 :
	    fscanf(fp_in,"%s =%i",s,&SNAP);
	    break;
	 case 25 :
	    fscanf(fp_in,"%s =%f",s,&TSNAP1);
	    break;
	 case 26 :
	    fscanf(fp_in,"%s =%f",s,&TSNAP2);
	    break;
	 case 27 :
	    fscanf(fp_in,"%s =%f",s,&TSNAPINC);
	    break;
	 case 28 :
	    fscanf(fp_in,"%s =%i",s,&SNAP_FORMAT);
	    break;
	 case 29 :
	    fscanf(fp_in,"%s =%s",s,SNAP_FILE);
	    break;
	 case 30 :
	    fscanf(fp_in,"%s =%i",s,&SEISMO);
	    break;
	 case 31 :
	    fscanf(fp_in,"%s =%i",s,&READREC);
	    break;
	 case 32 :
	    fscanf(fp_in,"%s =%s",s,REC_FILE);
	    break;
	 case 33 :
	    fscanf(fp_in,"%s =%f ,%f",s,&REFREC[1],&REFREC[2]);
	    break;
	 case 34 :
	    fscanf(fp_in,"%s =%f ,%f",s,&XREC1,&YREC1);
	    break;
	 case 35 :
	    fscanf(fp_in,"%s =%f ,%f",s,&XREC2,&YREC2);
	    break;
	 case 36 :
	    fscanf(fp_in,"%s =%i",s,&NGEOPH);
	    break;
	 case 37 :
	    fscanf(fp_in,"%s =%i",s,&REC_ARRAY);
	    break;
	 case 38 :
	    fscanf(fp_in,"%s =%f",s,&REC_ARRAY_DEPTH);
	    break;
	 case 39 :
	    fscanf(fp_in,"%s =%f",s,&REC_ARRAY_DIST);
	    break;
	 case 40 :
	    fscanf(fp_in,"%s =%i",s,&DRX);
	    break;
	 case 41 :
	    fscanf(fp_in,"%s =%i",s,&NDT);
	    break;
	 case 42 :
	    fscanf(fp_in,"%s =%i",s,&SEIS_FORMAT);
	    break;
	 case 43 :
	    fscanf(fp_in,"%s =%s",s,SEIS_FILE_VZ);
	    break;
	 case 44 :
	    fscanf(fp_in,"%s =%s",s,LOG_FILE);
	    break;     			
	 case 45 :
	    fscanf(fp_in,"%s =%i",s,&LOG);
	    break;
	 default:
	    break;
	 }
	 }
      }
   }
fclose(fp_in);

}
