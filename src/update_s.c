/*------------------------------------------------------------------------
 * Copyright (C) 2011 For the list of authors, see file AUTHORS.
 *
 * This file is part of SOFI2D_sh.
 * 
 * SOFI2D_sh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * SOFI2D_sh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SOFI2D_sh. See file COPYING and/or 
  * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
 *   updating stress components at gridpoints [nx1...nx2][ny1...ny2]
 *   by a staggered grid finite difference scheme of 4th order accuracy in space
 *   and second order accuracy in time
 *   last update 06/10/02, T. Bohlen
 *
 *  ----------------------------------------------------------------------*/

#include "fd.h"

void update_s(int nx1, int nx2, int ny1, int ny2,
float **  vz, float **   sxz, float **   syz,
float *** r, float *** p, float ** u, float ** taus, 
float *   etaip, float *   etajp, float * peta){


	int i,j,l;
	float bip, bjp, cip, cjp;
	float uip, ujp, tausip, tausjp;
	float  vzx, vzy, sumr=0.0, sump=0.0;
	float  dh24, dthalbe;	
	extern float DT, DH;
	extern int L, MYID;
	extern FILE *FP;
	double time1, time2;
	
	dthalbe=DT/2.0;
	dh24=1.0/(DH*24.0);
	
	if (MYID==0){
		time1=MPI_Wtime();
		fprintf(FP,"\n **Message from update_s (printed by PE %d):\n",MYID);
		fprintf(FP," Updating stress components ...");
	}	

	for (j=ny1;j<=ny2;j++){
		for (i=nx1;i<=nx2;i++){
			/* Compute values for shearmodulus u[j][i], 
			    P-wave modulus pi[j][i],
			    tau for S-waves and P-waves taus[j][i], 
			    taup[j][i] at staggered grid points: */

		       uip=0.5*(u[j][i+1]+u[j][i]);
		       ujp=0.5*(u[j+1][i]+u[j][i]);
		       tausip=0.5*(taus[j][i+1]+taus[j][i]);
		       tausjp=0.5*(taus[j+1][i]+taus[j][i]);
				 
		       
       
		       for (l=1;l<=L;l++){
			       etajp[l]=peta[l];
			       etaip[l]=peta[l];
		       }




		       /* spatial derivatives of particle velocity */
			vzx=(-vz[j][i+2]+27.0*(vz[j][i+1]-vz[j][i])+vz[j][i-1])*dh24;
			vzy=(-vz[j+2][i]+27.0*(vz[j+1][i]-vz[j][i])+vz[j-1][i])*dh24;
				
		       /* computing sums of the old memory variables */
		       sumr=sump=0.0;
		       for (l=1;l<=L;l++){
			       sumr+=r[j][i][l];
			       sump+=p[j][i][l];
		       }

			/* updating components of the stress tensor, partially */
			sxz[j][i]+=(uip*DT*(1.0+L*tausip)*vzx)+(dthalbe*sumr);
			syz[j][i]+=(ujp*DT*(1.0+L*tausjp)*vzy)+(dthalbe*sump);


		       /* now updating the memory-variables and sum them up*/
		       sumr=sump=0.0;
		       for (l=1;l<=L;l++){
			       bip=1.0/(1.0+(etaip[l]*0.5));
			       cip=1.0-(etaip[l]*0.5);
			       bjp=1.0/(1.0+(etajp[l]*0.5));
			       cjp=1.0-(etajp[l]*0.5);
			       r[j][i][l]=bip*(r[j][i][l]*cip-(uip*etaip[l]*tausip*vzx));
			       p[j][i][l]=bjp*(p[j][i][l]*cjp-(ujp*etajp[l]*tausjp*vzy));
			       sumr+=r[j][i][l];
			       sump+=p[j][i][l];
		       }

		       /* and now the components of the stress tensor are
		          completely updated */
			sxz[j][i]+=(dthalbe*sumr);
			syz[j][i]+=(dthalbe*sump);
		     
	       }
       }

	if (MYID==0){
		time2=MPI_Wtime();
		fprintf(FP," finished (real time: %4.2f s).\n",time2-time1);
	}
}
