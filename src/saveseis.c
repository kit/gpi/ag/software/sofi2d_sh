/*------------------------------------------------------------------------
 * Copyright (C) 2011 For the list of authors, see file AUTHORS.
 *
 * This file is part of SOFI2D_sh.
 * 
 * SOFI2D_sh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * SOFI2D_sh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SOFI2D_sh. See file COPYING and/or 
  * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
 *   write seismograms to files 
 *   last update 19/01/02, T. Bohlen
 *  ----------------------------------------------------------------------*/

#include "fd.h"

void saveseis(FILE *fp, float **sectionvz, int  **recpos, int  **recpos_loc, 
int ntr, float ** srcpos_loc, int nsrc,int ns){ 
		
	extern int  SEIS_FORMAT, MYID;	
	extern char  SEIS_FILE_VZ[STRING_SIZE];
		
	char myid[5];

	sprintf(myid,".%d",MYID);
	strcat(SEIS_FILE_VZ,myid);
	
	

	fprintf(fp," PE %d is writing %d seismograms (vz) to\n\t %s \n",MYID,ntr,SEIS_FILE_VZ);
	outseis(fp,fopen(SEIS_FILE_VZ,"w"),sectionvz,recpos,recpos_loc,ntr,srcpos_loc,nsrc,ns,SEIS_FORMAT);

}
