/*------------------------------------------------------------------------
 * Copyright (C) 2011 For the list of authors, see file AUTHORS.
 *
 * This file is part of SOFI2D_sh.
 * 
 * SOFI2D_sh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * SOFI2D_sh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SOFI2D_sh. See file COPYING and/or 
  * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/
/*
 *   Model homogeneous half space
 *   last update 11.04.02, T. Bohlen
 */

#include "fd.h"

void model(float  **  rho, float **  u, 
float **  taus, float *  eta){

	/*--------------------------------------------------------------------------*/
	/* extern variables */

	extern float DT, *FL, TAU, DH;
	extern int NX, NY, NXG, NYG,  POS[3], L, MYID;
	extern char  MFILE[STRING_SIZE];	

		/* local variables */
	float rhov, muv, vs, y, y1, y2;
	float *pts, ts, sumu, ws;
	int i, j, l, ii, jj, lines=0;
        char c;
	FILE *fp1, *fp2;
	 
	
	/*-----------------------------------------------------------------------*/

	const float VS1=240.0, RHO1=1800.0, Q1=30.0;
	const float VS2=430.0, RHO2=2000.0, Q2=30.0;
	const float VS3=1160.0, RHO3=2500.0, Q3=10000.0;

	/*-----------------------------------------------------------------------*/

	/* vector for maxwellbodies */
	pts=vector(1,L);
	for (l=1;l<=L;l++) {
		pts[l]=1.0/(2.0*PI*FL[l]);
		eta[l]=DT/pts[l];
	}


	ws=2.0*PI*FL[1];
		

        fp1=fopen("./model/b1i.dat","r");
        if (fp1==NULL) err(" could not open file b1i.dat ");
        fp2=fopen("./model/b2i.dat","r");
        if (fp2==NULL) err(" could not open file b2i.dat ");

        while ((c=fgetc(fp1)) != EOF)
                if (c=='\n') ++lines;
        rewind(fp1);

        if (MYID==0) printf(" PE %d: \t number of values: %d \n", MYID, lines);
	
	if (lines<NXG) err(" number of values must be greater than NXG!");
	
	

	/* loop over global grid */
		for (i=1;i<=NXG;i++){
			fscanf(fp1,"%e\n", &y1);
			fscanf(fp2,"%e\n", &y2);
			for (j=1;j<=NYG;j++){
			
  
  				y=(float)j*DH;
                		
				
				if (y<=y1){
					vs=VS1; rhov=RHO1; ts=2.0/Q1;}
				else if (y<=y2) {
					vs=VS2; rhov=RHO2; ts=2.0/Q2;}
				else {
					vs=VS3; rhov=RHO3; ts=2.0/Q3;}
					
                      
				sumu=0.0; 
				for (l=1;l<=L;l++){
					sumu=sumu+((ws*ws*pts[l]*pts[l]*ts)/(1.0+ws*ws*pts[l]*pts[l]));
				}
				
				muv=vs*vs*rhov/(1.0+sumu);

				/* only the PE which belongs to the current global gridpoint 
				  is saving model parameters in his local arrays */
				if ((POS[1]==((i-1)/NX)) && 
				    (POS[2]==((j-1)/NY))){
					ii=i-POS[1]*NX;
					jj=j-POS[2]*NY;

					taus[jj][ii]=ts;
					u[jj][ii]=muv;
					rho[jj][ii]=rhov;
				}
			}
		}	

		
	fclose(fp1);
	fclose(fp2);

	
	/* each PE writes his model to disk */

	writemod(MFILE,u,3);

	MPI_Barrier(MPI_COMM_WORLD);

	if (MYID==0) mergemod(MFILE,3);

	free_vector(pts,1,L);
}



