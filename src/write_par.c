/*------------------------------------------------------------------------
 * Copyright (C) 2011 For the list of authors, see file AUTHORS.
 *
 * This file is part of SOFI2D_sh.
 * 
 * SOFI2D_sh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * SOFI2D_sh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SOFI2D_sh. See file COPYING and/or 
  * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
 *   Write FD-Parameters to stdout                           
 *   last update 05/10/2002
 *
 *  T. Bohlen
 *  See COPYING file for copying and redistribution conditions.
 *  ----------------------------------------------------------------------*/

#include "fd.h"

/* printing all important parameters on stdout */
void write_par(FILE *fp){

	/* declaration of extern variables */
	extern int   NX, NY, NT, QUELLART;
	extern int  SNAP, SNAP_FORMAT, L, SRCREC;
	extern float DH, TIME, DT, TS, *FL, TAU, DAMPING, PLANE_WAVE_DEPTH, PHI;
	extern float REC_ARRAY_DEPTH, REC_ARRAY_DIST;
	extern float XREC1, XREC2, YREC1, YREC2;
	extern int SEISMO, NDT, NGEOPH, SEIS_FORMAT, FREE_SURF;
	extern int  READMOD, READREC, BOUNDARY, REC_ARRAY, DRX;
	extern float TSNAP1, TSNAP2, TSNAPINC, FW, REFREC[4];
	extern char SNAP_FILE[STRING_SIZE], SOURCE_FILE[STRING_SIZE], REC_FILE[STRING_SIZE];
	extern char SEIS_FILE_VZ[STRING_SIZE], SIGNAL_FILE[STRING_SIZE];
	extern char  MFILE[STRING_SIZE];
	extern int NP, NPROCX, NPROCY, MYID; 

	/* definition of local variables */
	int l;
	

	fprintf(fp,"\n **Message from write_par (printed by PE %d):\n",MYID);
	fprintf(fp,"\n");
	fprintf(fp,"------------------------- Processors ------------------------\n");
	fprintf(fp," Number of PEs in x-direction (NPROCX): %d\n",NPROCX);
	fprintf(fp," Number of PEs in vertical direction (NPROCY): %d\n",NPROCY);
	fprintf(fp," Total number of PEs in use: %d\n",NP);
	fprintf(fp,"\n");
	fprintf(fp," ----------------------- Discretization  ---------------------\n");
	fprintf(fp," Number of gridpoints in x-direction (NX): %i\n", NX);
	fprintf(fp," Number of gridpoints in y-direction (NY): %i\n", NY);
	fprintf(fp," Grid-spacing (DH): %e meter\n", DH);
	fprintf(fp," Time of wave propagation (T): %e seconds\n",TIME);
	fprintf(fp," Timestep (DT): %e seconds\n", DT);
	fprintf(fp," Number of timesteps: %i \n",NT);
	fprintf(fp,"\n");
	fprintf(fp," ------------------------- SOURCE -----------------------------\n");

	if (SRCREC){
		fprintf(fp," reading source positions, time delay, centre frequency \n");
		fprintf(fp," and initial amplitude from ASCII-file \n");
		fprintf(fp,"\t%s\n\n",SOURCE_FILE);
	} else {
		fprintf(fp," plane wave excitation: depth= %5.2f meter \n",PLANE_WAVE_DEPTH);
		fprintf(fp," incidence angle of plane P-wave (from vertical) PHI= %5.2f degrees \n",PHI);
 		fprintf(fp," duration of source signal: %e seconds\n",TS);
 		fprintf(fp," (centre frequency is approximately %e Hz)\n",1.0/TS);
	}


	fprintf(fp," wavelet of source:");

	switch (QUELLART){
	case 1 :
		fprintf(fp," Ricker\n");
		break;
	case 2 :
		fprintf(fp," Fuchs-Mueller\n");
		break;
	case 3 :
		fprintf(fp," reading from \n\t %s\n",SIGNAL_FILE);
		break;
	case 4 :
		fprintf(fp," sinus raised to the power of 3.0 \n");
		break;
	default :
		err(" Sorry, incorrect specification of source wavelet ! ");
	}

	fprintf(fp," Type of source: point source with directive force in  z-direction\n");
	
	fprintf(fp,"\n");

	if (SEISMO){
		fprintf(fp," ------------------------- RECEIVER  ------- -------------------\n");
		if (READREC){
			fprintf(fp," reading receiver positions from file \n");
			fprintf(fp,"\t%s\n\n",REC_FILE);
			fprintf(fp," reference_point_for_receiver_coordinate_system:\n");
			fprintf(fp," x=%f \ty=%f\t z=%f\n",REFREC[1], REFREC[2], REFREC[3]);
		} else if (REC_ARRAY>0){
				fprintf(fp," Horitontal lines of receivers.\n");
				fprintf(fp," number of lines: %d \n",REC_ARRAY);
				fprintf(fp," depth of upper line: %e m \n",REC_ARRAY_DEPTH);
				fprintf(fp," vertical increment between lines: %e m \n",REC_ARRAY_DIST);
				fprintf(fp," distance between receivers in x-direction within line: %i \n", DRX);		
		}else{

			fprintf(fp," first receiver position (XREC1,YREC1) = (%e, %e) m\n",
			    XREC1,YREC1);
			fprintf(fp," last receiver position (XREC1,YREC1) = (%e, %e) m\n",
			    XREC2,YREC2);
			fprintf(fp,"\n");
		}
	}

	fprintf(fp," ------------------------- FREE SURFACE ------------------------\n");
	if (FREE_SURF) fprintf(fp," free surface at the top of the model ! \n");
	else fprintf(fp," no free surface at the top of the model ! \n");
	fprintf(fp,"\n");

	fprintf(fp," ------------------------- ABSORBING FRAME ---------------------\n");
	if (FW>0.0){
		fprintf(fp," width of absorbing frame is %f m.\n",FW);
		fprintf(fp," Exponential damping applied. \n");
		fprintf(fp," Percentage of amplitude decay: %f .\n",DAMPING);
	}
	else fprintf(fp," absorbing frame not installed ! \n");


	switch (BOUNDARY){
	case 0 :
		fprintf(fp," No periodic boundary condition.\n");
		break;
	case 1 :
		fprintf(fp," Periodic boundary condition at left and right edges.\n");
		break;
	default :
		warning(" Wrong integer value for BOUNDARY specified ! ");
		warning(" No periodic boundary condition will be applied ");
		BOUNDARY=0;
		break;
	}

	if (READMOD){
		fprintf(fp," ------------------------- MODEL-FILES -------------------------\n");
		fprintf(fp," names of model-files: \n");
		fprintf(fp,"\t shear wave velocities:\n\t %s.vs\n",MFILE);
		fprintf(fp,"\t tau for shear waves:\n\t %s.ts\n",MFILE);
		fprintf(fp,"\t density:\n\t %s.rho\n",MFILE);
		for (l=1;l<=L;l++) fprintf(fp,"\t %1i. relaxation frequencies: %s.f%1i\n",l,MFILE,l);
	}

	fprintf(fp,"\n");
	fprintf(fp," ------------------------- Q-APROXIMATION --------------------\n");
	fprintf(fp," Number of relaxation mechanisms (L): %i\n",L);
	fprintf(fp," The L relaxation frequencies are at:  \n");
	for (l=1;l<=L;l++) fprintf(fp,"\t%f",FL[l]);
	fprintf(fp," Hz\n");
	fprintf(fp," Value for tau is : %f\n",TAU);


	if (SNAP){
		fprintf(fp,"\n");
		fprintf(fp," -----------------------  SNAPSHOTS  -----------------------\n");
		fprintf(fp," Snapshots of");
		switch(SNAP){
		case 1:
			fprintf(fp," z-component");
			fprintf(fp," of particle velocity.\n");
			break;
		default:
			err(" sorry, incorrect value for SNAP ! \n");
		}

		fprintf(fp," \t first (TSNAP1)= %8.5f s\n", TSNAP1);
		fprintf(fp," \t last (TSNAP2)=%8.5f s\n",TSNAP2);
		fprintf(fp," \t increment (TSNAPINC) =%8.5f s\n\n",TSNAPINC);
		fprintf(fp," \t first_and_last_horizontal(x)_gridpoint = %i, %i \n",1,NX);
		fprintf(fp," \t first_and_last_vertical_gridpoint = %i, %i \n",1,NY);
		fprintf(fp," \n name of output-file (SNAP_FILE):\n\t %s\n",SNAP_FILE);
		switch (SNAP_FORMAT){
		case 1 :
			err(" SU-Format not yet available !!");
			break;
		case 2 :
			fprintf(fp," The data is written in ASCII. \n");
			break;
		case 3 :
			fprintf(fp," The data is written binary (IEEE) (4 byte per float)");
			break;
		default:
			err(" Don't know the format for the Snapshot-data ! \n");
		}
	
		fprintf(fp,"\n\n");
	}
	if (SEISMO){
		fprintf(fp,"\n");
		fprintf(fp," -----------------------  SEISMOGRAMS  ----------------------\n");
		if ((SEISMO==1)){
			fprintf(fp," seismograms of ");
			fprintf(fp," z-component");
			fprintf(fp," of particle velocity.\n");
			fprintf(fp," output-file: \n ");
			fprintf(fp,"\t%s\n",SEIS_FILE_VZ);
		}
	
		switch (SEIS_FORMAT){
		case 1 :
			fprintf(fp," The data is written in IEEE SU-format . \n");
			break;
		case 2 :
			fprintf(fp," The data is written in ASCII. \n");
			break;
		case 3 :
			fprintf(fp," The data is written binary IEEE (4 byte per float)");
			break;
		default:
			err(" Sorry. I don't know the format for the seismic data ! \n");
		}
		fprintf(fp," samplingrate of seismic data: %f s\n",NDT*DT);
		if (!READREC) fprintf(fp," Trace-spacing: %5.2f m\n", NGEOPH*DH);
		fprintf(fp," Number of samples per trace: %i \n", iround(NT/NDT));
		fprintf(fp," ----------------------------------------------------------\n");
		fprintf(fp,"\n");
		fprintf(fp,"\n");
	}
	fprintf(fp,"\n");
	fprintf(fp," **************************************************************\n");
	fprintf(fp,"\n");

}
