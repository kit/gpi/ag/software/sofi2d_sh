/*------------------------------------------------------------------------
 * Copyright (C) 2011 For the list of authors, see file AUTHORS.
 *
 * This file is part of SOFI2D_sh.
 * 
 * SOFI2D_sh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * SOFI2D_sh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SOFI2D_sh. See file COPYING and/or 
  * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
 *   updating particle velocities at gridpoints [nx1...nx2][ny1...ny2]
 *   by a staggered grid finite difference scheme of 4th order accuracy in space
 *   and second order accuracy in time
 *   last update 06/10/02, T. Bohlen 
 *
 *  ----------------------------------------------------------------------*/

#include "fd.h"

void update_v(int nx1, int nx2, int ny1, int ny2, int nt,
float **  vz,  float ** sxz, float ** syz, float  ** rho,  
float **  srcpos_loc, float ** signals, int nsrc, float ** absorb_coeff){

	int i,j, l;
	float ripjm, dh24, amp;
	
	extern float DT, DH, FW;
	double time1, time2;
	extern int MYID;
	extern FILE *FP;


	dh24=1.0/(DH*24.0);

	if (MYID==0){
		time1=MPI_Wtime();
		fprintf(FP,"\n **Message from update_v (printed by PE %d):\n",MYID);
		fprintf(FP," Updating particle velocities ...");
	}


	for (j=ny1;j<=ny2;j++){
		for (i=nx1;i<=nx2;i++){
				
			/* updating the z-component of the velocity (vz) */
			vz[j][i] += DT*(dh24*(-sxz[j][i+1]+27.0*(sxz[j][i]-sxz[j][i-1])
			    +sxz[j][i-2]
			    -syz[j+1][i]+27.0*(syz[j][i]-syz[j-1][i])
			    +syz[j-2][i]))/rho[j][i];					 
		}
	}


	/* Adding body force components to corresponding particle velocities */
	for (l=1;l<=nsrc;l++) {
		i=(int)srcpos_loc[1][l];
		j=(int)srcpos_loc[2][l];
		amp=signals[l][nt];
		vz[j][i]+=amp;  /* single force in x */
	}
	
	if (FW>0.0)
	for (j=ny1;j<=ny2;j++){
		for (i=nx1;i<=nx2;i++){
			vz[j][i]*=absorb_coeff[j][i];
			sxz[j][i]*=absorb_coeff[j][i];
			syz[j][i]*=absorb_coeff[j][i];

		}
	}

	if (MYID==0){
		time2=MPI_Wtime();
		fprintf(FP," finished (real time: %4.2f s).\n",time2-time1);
	}
}
