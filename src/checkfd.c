/*------------------------------------------------------------------------
 * Copyright (C) 2011 For the list of authors, see file AUTHORS.
 *
 * This file is part of SOFI2D_sh.
 * 
 * SOFI2D_sh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * SOFI2D_sh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SOFI2D_sh. See file COPYING and/or 
  * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/
/*-------------------------------------------------------------
 *  Check FD-Grid for stability and grid dispersion.
 *  If the stability criterion is not fullfilled the program will
 *  terminate.                   
 *  last update  06/10/2002 
 *
 *  ----------------------------------------------------------*/


#include "fd.h"

void checkfd(FILE *fp, float ** prho, float ** pu,
float ** ptaus, float *peta){

	extern float DH, DT, TS, FW;
        extern int NX, NY, L, MYID;

	/* local variables */

	float  c, cmax_s=0.0, cmin_s=1e9, fmax;
	float  cmax=0.0, cmin=1e9, sum, dtstab, ts, cmax_r, cmin_r;
	const float w=2.0*PI/TS; /*center frequency of source*/
	int nfw=iround(FW/DH);
	int i, j, l, ny1=1, nx, ny;



	nx=NX; ny=NY; 

	/* low Q frame not yet applied as a absorbing boundary */
	/* if (!FREE_SURF) ny1=1+nfw;*/
	nfw=0;
	

	/* find maximum model phase velocity of shear waves at infinite
	      frequency within the whole model */
		for (i=1+nfw;i<=(nx-nfw);i++){
			for (j=ny1;j<=(ny-nfw);j++){
				c=sqrt(pu[j][i]*(1.0/prho[j][i])*(1.0+L*ptaus[j][i]));
				if (cmax_s<c) cmax_s=c;
				/* find minimum model phase velocity of shear waves at center
					       frequency of the source */
				sum=0.0;
				for (l=1;l<=L;l++){
					ts=DT/peta[l];
					sum=sum+((w*w*ptaus[j][i]*ts*ts)/(1.0+w*w*ts*ts));
				}
				c=sqrt((pu[j][i]*(1.0/prho[j][i]))*(1.0+sum));
				if (cmin_s>c) cmin_s=c;
			}
		}




	fprintf(fp,"\n\n\n **Message from checkfd (printed by PE %d):\n",MYID);
	fprintf(fp," Maximum and Minimum S-wave velocity within subvolume: \n ");
	fprintf(fp," MYID\t Vs_min(f=fc) \t Vsmax(f=inf) \n");
	fprintf(fp," %d \t %e \t %e \n", MYID,  cmin_s, cmax_s);

	cmax=cmax_s; 
	cmin=cmin_s; 

	/* find global maximum and global minimum for Vs*/
	MPI_Allreduce(&cmax,&cmax_r,1,MPI_FLOAT,MPI_MAX,MPI_COMM_WORLD);
	MPI_Allreduce(&cmin,&cmin_r,1,MPI_FLOAT,MPI_MIN,MPI_COMM_WORLD);
	cmax=cmax_r;
	cmin=cmin_r;	

		fprintf(fp," Global values for entire model: \n");
		fprintf(fp," Vs_max= %e m/s \t Vs_min=%e m/s \n\n", cmax,cmin);
		fprintf(fp,"\n\n ------------------ CHECK FOR GRID DISPERSION --------------------\n");
	      fprintf(fp," To satisfactorily limit grid dispersion the number of gridpoints \n");
	      fprintf(fp," per minimum wavelength (of S-waves) should be 6 (better more).\n");
	      fprintf(fp," Here the minimum wavelength is assumed to be minimum model phase velocity \n");
	      fprintf(fp," (of S-waves) at maximum frequency of the source\n");
	      fprintf(fp," devided by maximum frequency of the source.\n");
	      fmax=2.0/TS;
	      fprintf(fp," Maximum frequency of the source is approximately %8.2f Hz\n",2.0/TS);
	      fprintf(fp," The minimum wavelength (of S-waves) in the following simulation will\n");
	      fprintf(fp," be %e meter.\n", cmin/fmax);
	      fprintf(fp," Thus, the recommended value for DH is %e meter.\n", cmin/fmax/6.0);
	      fprintf(fp," You have specified DH= %e meter.\n\n", DH);
	      if (DH>(cmin/fmax/6.0))
	      warning(" Grid dispersion will influence wave propagation, choose smaller grid spacing (DH).");


	      fprintf(fp," \n\n ----------------------- CHECK FOR STABILITY ---------------------\n");
	      fprintf(fp," The following simulation is stable provided that\n\n");
	      fprintf(fp," \t p=cmax*DT/DH < 6/(7*sqrt(2)) = 0.60609,\n\n");
	      fprintf(fp," where cmax is the maximum phase velocity at infinite frequency,\n");

	      fprintf(fp," In the current simulation cmax is %8.2f m/s .\n\n",cmax);

	      fprintf(fp," DT is the timestep and DH is the grid size.\n\n");
	      dtstab=6.0*DH/(7.0*sqrt(2.0)*cmax);
	      fprintf(fp," In this simulation the stability limit for timestep DT is %e seconds .\n",dtstab);
	      fprintf(fp," You have specified DT= %e s.\n", DT);
	      if (DT>dtstab)
		      err(" The simulation will get unstable, choose smaller DT. ");
	      else fprintf(fp," The simulation will be stable.\n");


	      fprintf(fp,"\n\n ----------------------- ABSORBING BOUNDARY ------------------------\n");
	      fprintf(fp," Width (FW) of absorbing frame should be at least 30 gridpoints.\n");
	      fprintf(fp," You have specified a width of %d gridpoints.\n",iround(FW/DH));
	      if (iround(FW/DH)<30) 
		      warning(" Be aware of artificial reflections from grid boundaries ! \n");

}

