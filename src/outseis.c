/*------------------------------------------------------------------------
 * Copyright (C) 2011 For the list of authors, see file AUTHORS.
 *
 * This file is part of SOFI2D_sh.
 * 
 * SOFI2D_sh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * SOFI2D_sh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SOFI2D_sh. See file COPYING and/or 
  * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
 *   Write seismograms to disk                                  
 *   last update 19/01/02, T. Bohlen
 *  ----------------------------------------------------------------------*/
#include "fd.h"
#include "segy.h"

void  outseis(FILE *fp, FILE *fpdata, float **section,
int **recpos, int **recpos_loc, int ntr, float ** srcpos,
int nsrc, int ns, int seis_form){

	/* declaration of extern variables */
	extern int NDT;
	extern float  TIME, DH, DT, REFREC[4];

	const float xshift=800.0, yshift=800.0;


	/* declaration of local variables */
	int i,j;
	segy tr;
	int tracl ;
	float xr, yr, x, y;
	float XS=0.0, YS=0.0;
	
	
	
	if (nsrc==1){ 
		/* only if one source position is specified in SOURCE_FILE,  
			source coordinates are written into trace header fields */
		XS=srcpos[1][1];	
		YS=srcpos[2][1];	
	}	
	
	switch(seis_form){
	case 1 :
		for(tracl=1;tracl<=ntr;tracl++){        /*SEGY (without file-header)*/
         		xr=recpos[1][recpos_loc[3][tracl]]*DH;
         		yr=recpos[2][recpos_loc[3][tracl]]*DH;
         		x=xr-REFREC[1];
         		y=yr-REFREC[2];
			tr.tracl=(int)recpos_loc[3][tracl];     
			tr.cdp=(int)recpos_loc[3][tracl];
			tr.trid=(short)1;           /* trace identification code: 1=seismic*/
			tr.offset=(signed int)iround(sqrt((XS-xr)*(XS-xr)
                                  +(YS-yr)*(YS-yr))*1000.0);
			tr.gelev=(signed int)iround(yr*1000.0);
			tr.sdepth=(signed int)iround(YS*1000.0);   /* source depth (positive) */
                        /* angle between receiver position and reference point
                           (sperical coordinate system: swdep=theta, gwdep=phi) */
			tr.swdep=iround(((360.0/(2.0*PI))*atan2(x-xshift,y-yshift))*1000.0);
			tr.scalel=(signed short)-3;
			tr.scalco=(signed short)-3;
			tr.sx=(signed int)iround(XS*1000.0);  /* X source coordinate */

			        /* group coordinates */
			tr.gx=(signed int)iround(xr*1000.0);

			tr.ns=(unsigned short)ns; /* number of samples in this trace */
			tr.dt=(unsigned short)iround(((float)NDT*DT)*1.0e6); /* sample interval in micro-seconds */
			tr.d1=(float)(TIME/ns);        /* sample spacing for non-seismic data */

			for(j=1;j<=ns;j++) tr.data[j]=section[tracl][j];

			fwrite(&tr,240,1,fpdata);
			fwrite(&tr.data[1],4,ns,fpdata);
		}
		break;


	case 2 :
		for(i=1;i<=ntr;i++){         /*ASCII ONE COLUMN*/
			for(j=1;j<=ns;j++) fprintf(fpdata,"%e\n", section[i][j]);
		}
		break;

	case 3 :                             /*BINARY */

		for(i=1;i<=ntr;i++)
			for(j=1;j<=ns;j++){
				fwrite(&section[i][j],sizeof(float),1,fpdata); }
		break;

	default :
		fprintf(fp," Don't know data format for seismograms !\n");
		fprintf(fp," No output written. ");
	}

	fclose(fpdata);
}
