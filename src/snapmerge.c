/*------------------------------------------------------------------------
 * Copyright (C) 2011 For the list of authors, see file AUTHORS.
 *
 * This file is part of SOFI2D_sh.
 * 
 * SOFI2D_sh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * SOFI2D_sh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SOFI2D_sh. See file COPYING and/or 
  * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
 *   loop over snapshotfiles which have to be merged.                                   
 *   last update 06/10/02   T. Bohlen
 *
 *  ----------------------------------------------------------------------*/

#include "fd.h"
#include "globvar.h"      /* definition of global variables  */

int main(int argc, char **argv){

int nsnap;


/* read parameters from parameter-file (stdin) */
read_par(stdin); 

NXG=NX;
NYG=NY;	
NX = NXG/NPROCX;
NY = NYG/NPROCY;

nsnap=1+iround((TSNAP2-TSNAP1)/TSNAPINC);

FP=stdout;


merge(nsnap,3);


return 0;	

}
