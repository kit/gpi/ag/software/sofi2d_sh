/*------------------------------------------------------------------------
 * Copyright (C) 2011 For the list of authors, see file AUTHORS.
 *
 * This file is part of SOFI2D_sh.
 * 
 * SOFI2D_sh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * SOFI2D_sh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SOFI2D_sh. See file COPYING and/or 
  * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
 *   write values of dynamic field variables at the edges of the
 *   local grid into buffer arrays and  exchanged between
 *   processes.
 *   last update 21/09/02, T. Bohlen
 *
 *  ----------------------------------------------------------------------*/

#include "fd.h"

void exchange_s(float ** syz, float ** sxz, 
float ** bufferlef_to_rig, float ** bufferrig_to_lef, 
float ** buffertop_to_bot, float ** bufferbot_to_top){


	extern int NX, NY, POS[3], NPROCX, NPROCY, BOUNDARY, INDEX[5];
	extern const int TAG1,TAG2,TAG5,TAG6;
	MPI_Status  status;
	int i, j;

	if (POS[2]!=0)	/* no boundary exchange at top of global grid */
	for (i=1;i<=NX;i++){
			/* storage of top of local volume into buffer */
			buffertop_to_bot[i][1]  = syz[1][i];
	}


	if (POS[2]!=NPROCY-1)	/* no boundary exchange at bottom of global grid */
	for (i=1;i<=NX;i++){			
			/* storage of bottom of local volume into buffer */
			bufferbot_to_top[i][1]  = syz[NY][i];
			bufferbot_to_top[i][2]  = syz[NY-1][i];
			
	}
	
	
  	 /* send and reveive values for points at inner boundaries */
	MPI_Bsend(&buffertop_to_bot[1][1],NX*2,MPI_FLOAT,INDEX[3],TAG5,MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Recv(&buffertop_to_bot[1][1],NX*2,MPI_FLOAT,INDEX[4],TAG5,MPI_COMM_WORLD,&status);
	MPI_Bsend(&bufferbot_to_top[1][1],NX*2,MPI_FLOAT,INDEX[4],TAG6,MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Recv(&bufferbot_to_top[1][1],NX*2,MPI_FLOAT,INDEX[3],TAG6,MPI_COMM_WORLD,&status);   

	if (POS[2]!=NPROCY-1)	/* no boundary exchange at bottom of global grid */
	for (i=1;i<=NX;i++){
			syz[NY+1][i] = buffertop_to_bot[i][1];
	}

	if (POS[2]!=0)	/* no boundary exchange at top of global grid */
	for (i=1;i<=NX;i++){
			syz[0][i] = bufferbot_to_top[i][1];
			syz[-1][i] = bufferbot_to_top[i][2];
	}
	
	
	
	
	

	if ((BOUNDARY) || (POS[1]!=0))	/* no boundary exchange at left edge of global grid */
	for (j=1;j<=NY;j++){
			/* storage of left edge of local volume into buffer */
			bufferlef_to_rig[j][1] =  sxz[j][1];
	}


	if ((BOUNDARY) || (POS[1]!=NPROCX-1))	/* no boundary exchange at right edge of global grid */
	for (j=1;j<=NY;j++){
			/* storage of right edge of local volume into buffer */
			bufferrig_to_lef[j][1] =  sxz[j][NX];
			bufferrig_to_lef[j][2] =  sxz[j][NX-1];

	}	
	
	


 	 /* send and reveive values for points at inner boundaries */

 	MPI_Bsend(&bufferlef_to_rig[1][1],(NY)*2,MPI_FLOAT,INDEX[1],TAG1,MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Recv(&bufferlef_to_rig[1][1],(NY)*2,MPI_FLOAT,INDEX[2],TAG1,MPI_COMM_WORLD,&status);
	MPI_Bsend(&bufferrig_to_lef[1][1],(NY)*2,MPI_FLOAT,INDEX[2],TAG2,MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Recv(&bufferrig_to_lef[1][1],(NY)*2,MPI_FLOAT,INDEX[1],TAG2,MPI_COMM_WORLD,&status);


	if ((BOUNDARY) || (POS[1]!=NPROCX-1))	/* no boundary exchange at right edge of global grid */
	for (j=1;j<=NY;j++){

			sxz[j][NX+1] = bufferlef_to_rig[j][1];
		
	}

	if ((BOUNDARY) || (POS[1]!=0))	/* no boundary exchange at left edge of global grid */
	for (j=1;j<=NY;j++){
			sxz[j][0] = bufferrig_to_lef[j][1];
			sxz[j][-1] = bufferrig_to_lef[j][2];
	}
	
}
