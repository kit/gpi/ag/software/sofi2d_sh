/*------------------------------------------------------------------------
 * Copyright (C) 2011 For the list of authors, see file AUTHORS.
 *
 * This file is part of SOFI2D_sh.
 * 
 * SOFI2D_sh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * SOFI2D_sh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SOFI2D_sh. See file COPYING and/or 
  * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/
/*
 *   Model homogeneous half space
 *   last update 11.04.02, T. Bohlen
 */

#include "fd.h"

void model(float  **  rho, float **  u, 
float **  taus, float *  eta){

	/*--------------------------------------------------------------------------*/
	/* extern variables */

	extern float DT, *FL, TAU, DH;
	extern int NX, NY, NXG, NYG,  POS[3], L;
//	extern char  MFILE[STRING_SIZE];	

		/* local variables */
	float rhov, muv, vs, y;
	float *pts, ts, sumu, ws;
	int i, j, l, ii, jj;
	 
	
	/*-----------------------------------------------------------------------*/

	const float VS1=300.0, RHO1=1800.0, Q1=50.0;
	const float VS2=1800.0, RHO2=2500.0, Q2=200.0;
	const float Z1=20.0;

	/*-----------------------------------------------------------------------*/

	/* vector for maxwellbodies */
	pts=vector(1,L);
	for (l=1;l<=L;l++) {
		pts[l]=1.0/(2.0*PI*FL[l]);
		eta[l]=DT/pts[l];
	}


	ws=2.0*PI*FL[1];
		

	/* loop over global grid */
		for (i=1;i<=NXG;i++){
			for (j=1;j<=NYG;j++){
			
  
  				y=(float)j*DH;
				
				if (y<=Z1){
					vs=VS1; rhov=RHO1; ts=2.0/Q1;}
				else{
					vs=VS2; rhov=RHO2; ts=2.0/Q2;}
					
                      
				sumu=0.0; 
				for (l=1;l<=L;l++){
					sumu=sumu+((ws*ws*pts[l]*pts[l]*ts)/(1.0+ws*ws*pts[l]*pts[l]));
				}
				
				muv=vs*vs*rhov/(1.0+sumu);

				/* only the PE which belongs to the current global gridpoint 
				  is saving model parameters in his local arrays */
				if ((POS[1]==((i-1)/NX)) && 
				    (POS[2]==((j-1)/NY))){
					ii=i-POS[1]*NX;
					jj=j-POS[2]*NY;

					taus[jj][ii]=ts;
					u[jj][ii]=muv;
					rho[jj][ii]=rhov;
				}
			}
		}	

		

	
	/* each PE writes his model to disk */

/*	writemod(MFILE,pi,3);

	MPI_Barrier(MPI_COMM_WORLD);

	if (MYID==0) mergemod(MFILE,3);
*/
	free_vector(pts,1,L);
}



