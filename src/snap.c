/*------------------------------------------------------------------------
 * Copyright (C) 2011 For the list of authors, see file AUTHORS.
 *
 * This file is part of SOFI2D_sh.
 * 
 * SOFI2D_sh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * SOFI2D_sh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SOFI2D_sh. See file COPYING and/or 
  * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
 *   Write 2D snapshot for current timestep  to file                                   
 *   last update 06/10/2002
 *
 *  T. Bohlen
 *  See COPYING file for copying and redistribution conditions.
 *  ----------------------------------------------------------------------*/

#include "fd.h"


void snap(FILE *fp,int nt, int nsnap, float **vz){


	int i,j;
	char snapfile_z[STRING_SIZE], ext[8], wm[2];
	FILE *fpz;

	extern float DT;
	extern char SNAP_FILE[STRING_SIZE];
	extern int NX, NY,  SNAP_FORMAT;
	extern int MYID, POS[3];



	switch(SNAP_FORMAT){
	case 1:
		sprintf(ext,".su");
		break;
	case 2:
		sprintf(ext,".asc");
		break;
	case 3:
		sprintf(ext,".bin");
		break;
	}
	
	sprintf(snapfile_z,"%s%s.z.%i%i",SNAP_FILE,ext,POS[1],POS[2]);

	fprintf(fp,"\n\n PE %d is writing snapshot-data at T=%fs to \n",MYID,nt*DT);
	
	
	if (nsnap==1) 
		sprintf(wm,"w");
	else 
		sprintf(wm,"a");
		
	fprintf(fp,"%s\n", snapfile_z);
		
	fpz=fopen(snapfile_z,wm);
	for (i=1;i<=NX;i++)
	for (j=1;j<=NY;j++){
		writedsk(fpz,vz[j][i],SNAP_FORMAT);
	}
	
	fclose(fpz);

}


