/*------------------------------------------------------------------------
 * Copyright (C) 2011 For the list of authors, see file AUTHORS.
 *
 * This file is part of SOFI2D_sh.
 * 
 * SOFI2D_sh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * SOFI2D_sh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SOFI2D_sh. See file COPYING and/or 
  * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
 *   merge model files written by the different processes to 
 *   a single file                                 
 *   last update 16/02/02   T. Bohlen
 *
 *  ----------------------------------------------------------------------*/

#include "fd.h"


void mergemod(char modfile[STRING_SIZE], int format){



	extern int NXG, NYG, MYID, NPROCX, NPROCY;
	extern int NX, NY, NPROC;
	extern FILE *FP;


	char file[STRING_SIZE];
	FILE *fp;
	int i, j, ip, jp, ig, jg;
	float **field;




	fprintf(FP,"\n **Message from mergemod (printed by PE %d):\n",MYID);
	fprintf(FP," PE %d starts merge of %d model files \n",MYID,NPROC);	

	field =  matrix(1,NYG,1,NXG);

	fprintf(FP," Reading: ");

	for (ip=0;ip<=NPROCX-1; ip++)
	for (jp=0;jp<=NPROCY-1; jp++){


		sprintf(file,"%s.%i%i",modfile,ip,jp);
		fprintf(FP,"%s  ",file);
		fp=fopen(file,"r");
		if (fp==NULL) err("merge: can't read model file !"); 
		for (i=1;i<=NX;i++)
		for (j=1;j<=NY;j++){
			ig=i+ip*NX;
			jg=j+jp*NY;
			field[jg][ig]=readdsk(fp,format);
		}
		fclose(fp);
	}

	fprintf(FP,"\n PE %d is writing merged model file to  %s \n",MYID,modfile);
	fp=fopen(modfile,"w");
	for (i=1;i<=NXG;i++)
	for (j=1;j<=NYG;j++)
		writedsk(fp,field[j][i],format);

	fclose(fp);
	
	fprintf(FP," Use \n");
	fprintf(FP," ximage n1=%d < %s  label1=Y label2=X title=%s \n",
			NYG,modfile,modfile);
	fprintf(FP," to visualize model. \n");



}


