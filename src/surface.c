/*------------------------------------------------------------------------
 * Copyright (C) 2011 For the list of authors, see file AUTHORS.
 *
 * This file is part of SOFI2D_sh.
 * 
 * SOFI2D_sh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * SOFI2D_sh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SOFI2D_sh. See file COPYING and/or 
  * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
 *   stress free surface condition
 *   last update 06/10/02, T. Bohlen
 *
 *  ----------------------------------------------------------------------*/

#include "fd.h"

void surface(int ndepth, float ** syz){


	int i, j;
	
	extern int NX;


	j=ndepth;     /* The free surface is located exactly in y=j*dh ! */
	for (i=1;i<=NX;i++){
	
				
		/* mirroring of stress components for syz
		to make a stress free surface in depth y=jsurf*h */
		syz[j][i]=-syz[j+1][i];
		syz[j-1][i]=-syz[j+2][i];
	
	

	}
}
