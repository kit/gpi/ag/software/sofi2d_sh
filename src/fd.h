/*------------------------------------------------------------------------
 * Copyright (C) 2011 For the list of authors, see file AUTHORS.
 *
 * This file is part of SOFI2D_sh.
 * 
 * SOFI2D_sh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * SOFI2D_sh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SOFI2D_sh. See file COPYING and/or 
  * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
 *  fd.h - include file for viscoelastic FD programs (SH-wave modelling)         
 *  last update  06/10/2002 
 *
 *  Copyright (c) 1998 T. Bohlen 
 *  See COPYING file for copying and redistribution conditions.
 *  ---------------------------------------------------------------------*/

/* files to include */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <time.h>
#include <mpi.h>

#define iround(x) ((int)(floor)(x+0.5))
#define min(x,y) ((x<y)?x:y)    
#define max(x,y) ((x<y)?y:x)
#define fsign(x) ((x<0.0)?(-1):1)    

#define PI (3.141592653589793)
#define NPAR 31
#define STRING_SIZE 74
#define REQUEST_COUNT 4


/* declaration of functions */

int **splitrec(int **recpos,int *ntr_loc, int ntr);

void absorb(float ** absorb_coeff);

void checkfd(FILE *fp, float ** prho, float ** pu,
float ** ptaus, float *peta);

void exchange_v(float ** vz,  
float ** bufferlef_to_rig, float ** bufferrig_to_lef, 
float ** buffertop_to_bot, float ** bufferbot_to_top);

void exchange_s(float ** syz, float ** sxz, 
float ** bufferlef_to_rig, float ** bufferrig_to_lef, 
float ** buffertop_to_bot, float ** bufferbot_to_top);

void exchange_par(void);

void info(FILE *fp);

void initproc(void);

void model(float  **  rho, float **  u, 
float **  taus, float *  eta);

void matcopy(float ** prho, float ** pu, float ** ptaus);

void merge(int nsnap, int type);

void merge2(int nsnap, int type);

void mergemod(char modfile[STRING_SIZE], int format);

void note(FILE *fp);

void  outseis(FILE *fp, FILE *fpdata, float **section,
int **recpos, int **recpos_loc, int ntr, float ** srcpos_loc,
int nsrc, int ns, int seis_form);


float *rd_sour(int *nts,FILE* fp_source);

float readdsk(FILE *fp_in, int format);

void read_par(FILE *fp_in);

int **receiver(FILE *fp, int *ntr);

void saveseis(FILE *fp, float **sectionvz, int  **recpos, int  **recpos_loc, 
int ntr, float ** srcpos_loc, int nsrc,int ns);

void snap(FILE *fp,int nt, int nsnap, float **vz);

void snapmerge(int nsnap);

void snapmerge2(int nsnap);

float **sources(int *nsrc);

void seismo(int lsamp, int ntr, int **recpos, float **sectionvz, float **vz);

float **splitsrc(float **srcpos,int *nsrc_loc, int nsrc);

void surface(int ndepth, float ** psyz);

void update_s(int nx1, int nx2, int ny1, int ny2,
float **  vz, float **   sxz, float **   syz,
float *** r, float *** p, float ** u, float ** taus, 
float *   etaip, float *   etajp, float * peta);

void update_v(int nx1, int nx2, int ny1, int ny2, int nt,
float **  pvz, float ** psxz, float ** psyz, float  ** prho,  
float **  srcpos_loc, float ** signals, int nsrc, float ** absorb_coeff);

float ** wavelet(float ** srcpos_loc, int nsrc);

void write_par(FILE *fp);

void writedsk(FILE *fp_out, float amp, int format);

void writemod(char modfile[STRING_SIZE], float ** array, int format);

/* utility functions */
void err(char err_text[]);
void warning(char warn_text[]);
double maximum(float **a, int nx, int ny);
float *vector(int nl, int nh);
float **matrix(int nrl, int nrh, int ncl, int nch);
int **imatrix(int nrl, int nrh, int ncl, int nch);
float ***f3tensor(int nrl, int nrh, int ncl, int nch,int ndl, int ndh);
void free_vector(float *v, int nl, int nh);
void free_ivector(int *v, int nl, int nh);
void free_matrix(float **m, int nrl, int nrh, int ncl, int nch);
void free_imatrix(int **m, int nrl, int nrh, int ncl, int nch);
void free_f3tensor(float ***t, int nrl, int nrh, int ncl, int nch, int ndl, 
int ndh);
