/*------------------------------------------------------------------------
 * Copyright (C) 2011 For the list of authors, see file AUTHORS.
 *
 * This file is part of SOFI2D_sh.
 * 
 * SOFI2D_sh is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.0 of the License only.
 * 
 * SOFI2D_sh is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SOFI2D_sh. See file COPYING and/or 
  * <http://www.gnu.org/licenses/gpl-2.0.html>.
--------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
 *   globvar.h - global variables of viscoelastic FD programs   
 *   last update 05/04/02 T. Bohlen
 *
 *  ----------------------------------------------------------------------*/

/* definition of global variables used in the finite difference programs*/
/* generally, for the names of the global variables
   uppercase letters are used */

float XS, YS, DH, TIME, DT, TS, DAMPING, PLANE_WAVE_DEPTH, PHI;
float TSNAP1, TSNAP2, TSNAPINC, FW=0.0, *FL, TAU;
float XREC1, XREC2, YREC1, YREC2;
float REC_ARRAY_DEPTH, REC_ARRAY_DIST;
float REFREC[4]={0.0, 0.0, 0.0, 0.0};
int   SEISMO, NDT, NGEOPH, NSRC=1, SEIS_FORMAT, FREE_SURF, READMOD, READREC, SRCREC;
int   NX, NY, NT, QUELLART, SNAP, SNAP_FORMAT, LOG, REC_ARRAY;
int   L, BOUNDARY, DC, DRX, NXG, NYG;
char  SNAP_FILE[STRING_SIZE], SOURCE_FILE[STRING_SIZE], SIGNAL_FILE[STRING_SIZE];
char  MFILE[STRING_SIZE], REC_FILE[STRING_SIZE];
char  SEIS_FILE_VZ[STRING_SIZE],  LOG_FILE[STRING_SIZE];
FILE *FP;

/* Mpi-variables */
int   NP, NPSP, NPROC, NPROCX, NPROCY, MYID, IENDX, IENDY;
int   POS[3], INDEX[5];     
const int TAG1=1,TAG2=2, TAG3=3, TAG4=4, TAG5=5,TAG6=6; 

