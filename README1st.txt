Dear SOFI2D_sh user,

please note, that SOFI2D_sh is in a very unsupported alpha-stage. The code comes
as it is, no manual is provided (though it would be similar to the SOFI2D
guide), no tests have been performed. We also do not use this code for
production. Please expect problems running the
code! 

This release should merely demonstrate the modeling of sh-waves.
Feel free to modify the code (to make it running again) or to fix occuring bugs.
We just kindly ask you to report any fixes.

Thanks a lot in advance.
